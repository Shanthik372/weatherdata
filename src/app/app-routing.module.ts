import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CityWeatherComponent } from './weather/city-weather/city-weather.component';
import { HomeComponent } from './home/home.component';
import { WeatherComponent } from './weather/weather.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'weather', component: WeatherComponent },
  { path: 'citydetails', component: CityWeatherComponent },
  { path: '', component: HomeComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
