import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  
  api = '73df4bb596fb686754b32d679149f01e';
  
  cityname1 = 'Florence';
  cityname2 = 'Vienna';
  cityname3 = 'Edinburgh';
  cityname4 = 'Prague';
  cityname5 = 'Paris';

  city1;
  city2;
  city3;
  city4;
  city5;

  constructor() {}

  ngOnInit() {
    this.setCities();
    this.getWeather();
  }

  setCities() {

    if (window.localStorage) {

      if (window.localStorage.getItem('cityname1')) {
        this.cityname1 = window.localStorage.getItem('cityname1');
      } else {
        window.localStorage.setItem('cityname1', this.cityname1);
      }
      if (window.localStorage.getItem('cityname2')) {
        this.cityname2 = window.localStorage.getItem('cityname2');
      } else {
        window.localStorage.setItem('cityname2', this.cityname2);
      }
      if (window.localStorage.getItem('cityname3')) {
        this.cityname3 = window.localStorage.getItem('cityname3');
      } else {
        window.localStorage.setItem('cityname3', this.cityname3);
      }
      if (window.localStorage.getItem('cityname4')) {
        this.cityname4 = window.localStorage.getItem('cityname4');
      } else {
        window.localStorage.setItem('cityname4', this.cityname4);
      }
      if (window.localStorage.getItem('cityname5')) {
        this.cityname5 = window.localStorage.getItem('cityname5');
      } else {
        window.localStorage.setItem('cityname5', this.cityname5);
      }

    }
  }

  getWeather() {
    
    const ajax1 = new XMLHttpRequest();
    ajax1.open('GET', 'https://api.openweathermap.org/data/2.5/weather?q=' + this.cityname1 + '&appid=' + this.api, false);
    ajax1.send();
    this.city1 = JSON.parse(ajax1.responseText);

    const ajax2 = new XMLHttpRequest();
    ajax2.open('GET', 'https://api.openweathermap.org/data/2.5/weather?q=' + this.cityname2 + '&appid=' + this.api, false);
    ajax2.send();
    this.city2 = JSON.parse(ajax2.responseText);

    const ajax3 = new XMLHttpRequest();
    ajax3.open('GET', 'https://api.openweathermap.org/data/2.5/weather?q=' + this.cityname3 + '&appid=' + this.api, false);
    ajax3.send();
    this.city3 = JSON.parse(ajax3.responseText);

    const ajax4 = new XMLHttpRequest();
    ajax4.open('GET', 'https://api.openweathermap.org/data/2.5/weather?q=' + this.cityname4 + '&appid=' + this.api, false);
    ajax4.send();
    this.city4 = JSON.parse(ajax4.responseText);

    const ajax5 = new XMLHttpRequest();
    ajax5.open('GET', 'https://api.openweathermap.org/data/2.5/weather?q=' + this.cityname5 + '&appid=' + this.api, false);
    ajax5.send();
    this.city5 = JSON.parse(ajax5.responseText);
  }

}
