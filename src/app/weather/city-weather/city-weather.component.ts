import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-city-weather',
  templateUrl: './city-weather.component.html',
  styleUrls: ['./city-weather.component.css']
})
export class CityWeatherComponent implements OnInit {

  api = '73df4bb596fb686754b32d679149f01e';
  fiveDay = [];
  forecast;
  CityName;

  constructor(private location: Location, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.CityName = params.CityName
    })
    const ajax = new XMLHttpRequest();
    ajax.open('GET', 'https://api.openweathermap.org/data/2.5/forecast?q=' + this.CityName + '&appid=' + this.api, false);
    ajax.send();
    this.fiveDay = [];
    this.forecast = JSON.parse(ajax.responseText);
    if (this.forecast.list) {
      let i;
      for (i = 7; i < this.forecast.list.length; i = i + 8) {
        this.fiveDay.push(this.forecast.list[i]);
      }
    }
  }

  convert(kelvin) {
    return Math.round((kelvin - 273) * (1.8) + 32);
  }

  convertDay(day) {
    const newDay = day.dt_txt.replace(' ', 'T');
    const date = new Date(newDay);
    const days = ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'];
    return days[date.getDay()];
  }

  getClass(name) {
    if (name === 'Clear') {
      return 'fa-sun';
    } else if (name === 'Clouds') {
      return 'fa-cloud-sun';
    } else if (name === 'Rain') {
      return 'fa-cloud-showers-heavy';
    } else if (name === 'Snow') {
      return 'fa-snowflake';
    }
  }

  getPath() {
    this.location.back();
  }

}
