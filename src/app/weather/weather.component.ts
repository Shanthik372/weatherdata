import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.css']
})

export class WeatherComponent {

  api = '73df4bb596fb686754b32d679149f01e';

  @Input() city;
  @Input() index;

  constructor(public router?: Router) { }

  convert(kelvin) {
    return Math.round((kelvin - 273) * (1.8) + 32);
  }

  expand() {
    this.router.navigate(['/citydetails'], {queryParams: {CityName: this.city.name}});
  }

  getTime(val) {
    const date = new Date(0);
    date.setUTCSeconds(val);
    let hours = date.getHours();
    const minutes = this.minutes_with_leading_zeros(date);
    if (hours > 12) {
      hours = hours - 12;
    }
    return hours + ':' + minutes;
  }

  minutes_with_leading_zeros(date) {
    return (date.getMinutes() < 10 ? '0' : '') + date.getMinutes();
  }

}
